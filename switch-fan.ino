/*
  Input Pull-up Serial

  This example demonstrates the use of pinMode(INPUT_PULLUP). It reads a digital
  input on pin 2 and prints the results to the Serial Monitor.

  The circuit:
  - momentary switch attached from pin 2 to ground
  - built-in LED on pin 13

  Unlike pinMode(INPUT), there is no pull-down resistor necessary. An internal
  20K-ohm resistor is pulled to 5V. This configuration causes the input to read
  HIGH when the switch is open, and LOW when it is closed.

  created 14 Mar 2012
  by Scott Fitzgerald

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/InputPullupSerial
*/

int sensePin = A1;  //This is the Arduino Pin that will read the sensor output
int sensorInput;    //The variable we will use to store the sensor input
double temp;        //The variable we will use to store temperature in degrees. 


boolean fanState = false; //fan starts off

void setup() {
  //start serial connection
  Serial.begin(9600);
  //configure pin 2 as an input and enable the internal pull-up resistor
  //http://www.circuitstoday.com/arduino-nano-tutorial-pinout-schematics
  //pin d2 is pin 5
  //pin d3 is pin 6
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
}


/**
 * Turns the fans on or off
 * 
 * state: true is on false is off
*/ 
void toggleFan( boolean state ){
  if( fanState != state ){
     if( state ){
        digitalWrite(2, LOW);
        digitalWrite(3, LOW);
     }else{
        digitalWrite(2, HIGH);
        digitalWrite(3, HIGH);
     }
     fanState = state;
  }
}

/**
 * This doesn't currently work.
 * 
 * sources:
 * - https://www.bc-robotics.com/tutorials/using-a-tmp36-temperature-sensor-with-arduino/
 */
void readTemp(){
  // put your main code here, to run repeatedly: 
  sensorInput = analogRead(sensePin);        //read the analog sensor and store it
  temp = (double)sensorInput / 1024;   //find percentage of input reading
  temp = temp * 5;                     //multiply by 5V to get voltage
  temp = temp - 0.5;                   //Subtract the offset 
  temp = temp * 100;                   //Convert to degrees
  Serial.print("tmp: ");
  Serial.println( temp );
  Serial.print("decimal: ");
  Serial.println( sensorInput );
}


void loop() {
  //readTemp();
<<<<<<< HEAD
  //delay(100);
  toggleFan( true );
  
  Serial.print( "true" );
  Serial.println( temp );
  delay( 1000UL * 60UL * 1UL ); // turn on for 1 min
  //readTemp();
  toggleFan( false );  
  Serial.print( "false" );
  Serial.println( String(temp) );
  delay( 1000UL * 60UL * 5UL );//turn off for 5 min
  
=======
  toggleFan( false );  
  //Serial.print( "false" );
  //Serial.println( String(temp) );
  delay( 1000 * 1 );//turn off for 10 min
  readTemp();
  delay(100);
  toggleFan( true );
  
  //Serial.print( "true" );
  //Serial.println( temp );
  delay( 1000 * 1 ); // turn on for 1 min
>>>>>>> 8ba05ca1e8ba83b8f8a826ff7e528e4876d43495

}
