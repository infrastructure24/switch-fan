# switch-fan

Arduino project for controlling the fan on a cisco catalyst 2900 switch

## Hardware Diagram

```mermaid
graph TD
	A[Arduino] -->|reads| T(Temp Sensor)
	A --> |pin 1| R1(Relay)
	A --> |pin 2| R2(Relay)
```

## Hardware description

Here's the hardware I'm using

### Arduino Nano

- Board number: XCSOURCE 5x V3.0 USB Nano ATmega328P 5V 16M Micro Controller Board Module for Arduino (TE359) 
  - This is the name from amazon, unfortunately they no longer sell the part, so I can't give a link. (this might be the new link? https://www.amazon.co.uk/XCSOURCE-ATmega328P-Controller-Arduino-TE359/dp/B015MGHH6Q)

- These require a driver on windows that can be found automatically through device manager.

#### Uploading

- To upload to this board follow this section
- specify:
  - the correct usb port in `tools/ports`
  - `Arduino Nano` in `tools/board`
  - `ATmega328p (old bootloader)` in `tools/processor`

- Reset the board just before the sketch is uploaded (generally after compiling).
- These steps were taken from [stack overflow](https://stackoverflow.com/questions/19765037/arduino-sketch-upload-issue-avrdude-stk500-recv-programmer-is-not-respondi)

### Relay

- Board number:
- https://www.amazon.com/gp/product/B07M88JRFY/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&th=1
- JQC-3FF-S-Z

Specification:
Material: Circuit board 
Control signal: TTL 
Rated load: AC125~250V/10A, DC28~30V/10A 
Max. switch voltage: 250VAC, 30V
  Trigger Voltage: 0-1.5V (LOW); 3-5V (High)
Trigger Current: 5mA
Max. Current: 190mA
Size: 50mm * 26mm * 18.5mm (1.9 inch *1.02 inch * 0.72 inch )

Input Connection:
DC +: Positive power supply (VCC)
DC-: Connect power negative ( GND)
IN: Control the pick up of replay by low level or high level

Output Connection:
Left Connection: “NO” means “Normally Open”
Middle Connection: “COM” means “Common”
Right Connection: “NC” means “Normally Closed”

High and Low Level Trigger Effective
High level trigger when the jumper cable connecting “High” to Short Circuit;
Low level trigger when the jumper cable connecting “Low” to Short Circuit;

LED Indicator Light:
Power Indicator: Green
Indicator of Relay Status: Red

### Temp sensor

- Part number: TMP36
- https://www.bc-robotics.com/tutorials/using-a-tmp36-temperature-sensor-with-arduino/
